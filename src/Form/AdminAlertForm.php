<?php

namespace Drupal\login_form_alert\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AdminAlertForm.
 */
class AdminAlertForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'login_form_alert.adminalert',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'admin_alert_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('login_form_alert.adminalert');
    $form['alert_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Alert Message'),
      '#maxlength' => 250,
      '#size' => 150,
      '#default_value' => $config->get('alert_message'),
    ];
    $form['enable_for_display'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable for display'),
      '#default_value' => $config->get('enable_for_display'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('login_form_alert.adminalert')
      ->set('alert_message', $form_state->getValue('alert_message'))
      ->set('enable_for_display', $form_state->getValue('enable_for_display'))
      ->save();
  }

}
